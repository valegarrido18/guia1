#include <iostream>
#include <stdlib.h>
#include "Profesor.h" 

using namespace std;

int main (){
	int dimensionarr=3;
	string nombre = "\0";
	string sexo = "\0";
	int edad = 0;
	int promedio = 0;
	
	/* arreglo */ 
	Profesor profesores[dimensionarr];
	cout << "llene el registro de profesores: " << endl;
	/* recorrer arreglo */ 
	for (int i = 0; i < dimensionarr; i++){
		cout << "nombre: " << endl;
		cin >> nombre;
		cout << "sexo: " << endl;
		cin >> sexo;
		cout << "edad: " << endl;
		cin >> edad;
		profesores[i].set_nombre(nombre);
		profesores[i].set_sexo(sexo);
		profesores[i].set_edad(edad);
		promedio = promedio + profesores[i].get_edad();
	} 
	
	system("clear");

	promedio = promedio /dimensionarr;
	cout << "Promedio profesores: " << promedio << endl;
	
	
	cout << "Profesores con la edad menor al promedio: " << endl;
	for (int i = 0; i < dimensionarr; i++){
	
		if (profesores[i].get_edad()  < promedio){
			cout << profesores[i].get_nombre() << endl;
		}
	}
	cout << "Profesores con la edad mayor al promedio: " << endl;
	for (int i = 0; i < dimensionarr; i++){
	
		if (profesores[i].get_edad()  > promedio){
			cout << profesores[i].get_nombre() << endl;
		}
	}
	
	/* ordenamiento */ 
	Profesor temporal; 
	for(int j=1;j<=dimensionarr;j++){
		for(int i=0;i<dimensionarr-1;i++){
			if(profesores[i].get_edad()>profesores[i+1].get_edad()){
				temporal=profesores[i];
				profesores[i]=profesores[i+1];
				profesores[i+1]=temporal;
			}
		}
	}
	
	cout <<"Profesor de menor edad  " << profesores[0].get_nombre() << endl;
	cout <<"Profesor de mayor mayor  "<< profesores[dimensionarr-1].get_nombre() << endl;
	
	return 0;
}
