>> Ejercicio 1 <<

El ejercicio consiste en ingresar profesores con sus datos respectivos de cada uno de ellos, ya sea nombre, edad y sexo. luego se mostrará por pantalla el promedio de la edad de todos los profesores que fueron ingresados, el nombre del profesor con menor edad, el de mayor edad y por ultimo a los profesores que tengan edad superior e inferior al promedio de todos ellos.

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa para proceder a la ejecucion del programa. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
