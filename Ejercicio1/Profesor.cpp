#include <iostream>
#include <stdlib.h>
#include "Profesor.h" 

using namespace std;

/* constructor */
Profesor::Profesor(){ 
}

/* getter and setter */ 
string Profesor::get_nombre(){
	return this->nombre;
}

void Profesor::set_nombre(string nombre){
	this->nombre = nombre;
}

string Profesor::get_sexo(){
	return this->sexo;
}

void Profesor::set_sexo(string sexo){
	this->sexo = sexo;
}

int Profesor::get_edad(){
	return this->edad;
}

void Profesor::set_edad(int edad){
	this->edad = edad;
}

